/*! Gauge v1.0 | Code under MIT License | Copyright (c) Ajabep */


var Gauge = function (howMany, labels) {
	'use strict';
	
	howMany = howMany || 1;
	labels = labels || [];
	
	if (howMany < 1)
		throw new Error('You must request at least 1 bar');
	
	var container = document.createElement('div');
	
	container.requestingNewValue = false;
	container.classList.add('gauge');
	
	
	// params
	container.bar = [];
	
	var bar,
		defaultLabel = 'state';
	for (var i = 0 ; i < howMany ; ++i) {
		
		bar = document.createElement('div');
		
		bar.setAttribute('role', 'progressbar');
		bar.setAttribute('aria-valuemax', '100');
		bar.setAttribute('aria-valuemax', '0');
		
		bar.label = labels[i] || defaultLabel;
		
		bar.setStart = function (start) {

			var progress = this.getProgress();
			
			if (start < 0 || start > 100) {
				throw new Error('the first param must be between 0 and 100');
			}

			if (start !== parseFloat(start, 10)) {
				throw new Error('the first param must be a float');
			}
			
			if (start > progress) {
				throw new Error('the start must lower or equals than the progress');
			}
			
			this._setStart(start);
			this._setProgress(progress - start);
			
			this._updateLabel();
		};
		
		bar.setProgress = function (progress) {

			var start = this.start;
			
			if (progress < 0 || progress > 100) {
				throw new Error('the first param must be between 0 and 100');
			}

			if (progress !== parseFloat(progress, 10)) {
				throw new Error('the first param must be a float');
			}
			
			if (start > progress) {
				throw new Error('the progress must greater or equals than the start');
			}
			
			this._setProgress(progress - start);
			this._updateLabel();
		};
		
		bar.setLabel = function (label) {
			this.label = label || defaultLabel;
			this._updateLabel();
		}
		
		bar._setStart = function (start) {
			this.start = start;
			this.style.left = start + "%";
		};
		
		bar._setProgress = function (progress) {
			this.progress = progress;
			this.style.width = progress + "%";
		};
		
		bar._updateLabel = function () {
			var progress = Math.round(this.progress);
			
			if (this.start) {
				bar.setAttribute('aria-labelledby', this.label + ': ' + Math.round(this.start) + ' to ' + progress + ' %');
				bar.setAttribute('aria-valuetext', this.label + ': ' + Math.round(this.start) + ' to ' + progress + ' %');
			}
			else {
				bar.setAttribute('aria-labelledby', this.label + ': ' + progress + '%');
				bar.setAttribute('aria-valuetext', this.label + ': ' + progress + '%');
			}
			
			bar.setAttribute('aria-valuenow', progress);
		};
		
		bar.getStart = function () {
			return this.start;
		};
		
		bar.getProgress = function () {
			return this.progress + this.start;
		};
		
		bar.getLabel = function () {
			return this.label;
		}
		
		
		
		container.appendChild(bar);
		container.bar.push(bar);
		
		// Init
		bar.setStart(0);
		bar.setProgress(0);
	}
	
	container.setProgress = function (progress, numBar) {
		
		numBar = numBar || 0;
		
		if (progress < 0 || progress > 100) {
			throw new Error('the first param must be between 0 and 100');
		}
		
		if (progress !== parseFloat(progress, 10)) {
			throw new Error('the first param must be a float');
		}
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar >= this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		this.bar[numBar].setProgress(progress);
	};
	
	container.setStart = function (start, numBar) {
		
		numBar = numBar || 0;
		
		if (start < 0 || start > 100) {
			throw new Error('the first param must be between 0 and 100');
		}
		
		if (start !== parseFloat(start, 10)) {
			throw new Error('the first param must be a float');
		}
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar >= this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		this.bar[numBar].setStart(start);
	};
	
	container.setLabel = function (label, numBar) {
		
		numBar = numBar || 0;
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar >= this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		this.bar[numBar].setLabel(label);
	};
	
	container.getProgress = function (numBar) {
		
		numBar = numBar || 0;
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar > this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		return this.bar[numBar].getProgress();
	};
	
	container.getStart = function (numBar) {
		
		numBar = numBar || 0;
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar > this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		return this.bar[numBar].getStart();
	};
	
	container.getLabel = function (numBar) {
		
		numBar = numBar || 0;
		
		if (numBar < 0)
			throw new Error('The index of bar must be >= 0');
		
		if (numBar > this.bar.length)
			throw new Error('The bar number ' + numBar + ' not exist');
		
		return this.bar[numBar].getLabel();
	};
	
	container.addEventListener('mousedown', function(e){
		container.requestingNewValue = true;
		container.classList.add('requestingNewValue');
		
		var request = new CustomEvent('requestValueStart', {});
		container.dispatchEvent(request);
		
		container.setAttribute('aria-busy', true);
		
		container.throwRequestValue(e);
	});
	
	window.addEventListener('mouseup', function(e){
		if (container.requestingNewValue) {
			container.classList.remove('requestingNewValue');
			container.requestingNewValue = false;
		
			var request = new CustomEvent('requestValueStop', {});
			container.dispatchEvent(request);
			container.setAttribute('aria-busy', false);
		}
	});
	
	window.addEventListener('mousemove', function(e){
		if (container.requestingNewValue)
			container.throwRequestValue(e);
	});
							   
	container.throwRequestValue = function (e) {
		
		var objSizeAndPos = container.getBoundingClientRect();
		
		var newValue = e.clientX - (objSizeAndPos.left + window.pageXOffset);
		
		newValue /= Math.round(objSizeAndPos.width);
		newValue *= 100;
		
		if (newValue < 0)
			newValue = 0;
		
		else if (newValue > 100)
			newValue = 100;
	
		 
		var request = new CustomEvent(
			'requestValue',
			{
				'detail': {
					'newValue': newValue
				}
			}
		);
		this.dispatchEvent(request);
	};
	
	return container;
	
};