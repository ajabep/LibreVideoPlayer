/*! LibreVideoPlayer v1.0 | Code under MIT License | Copyright (c) Ajabep */



/******** requestAnimationFrame polyfill ******/
// https://gist.github.com/paulirish/1579671
(function() {
	
	'use strict';
	
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());




/*********** fullscreen ***************/
var FullscreenApi = {};

// browser API methods
// map approach from Screenful.js - https://github.com/sindresorhus/screenfull.js
var apiMap = [
	// Spec: https://dvcs.w3.org/hg/fullscreen/raw-file/tip/Overview.html
	[
		'requestFullscreen',
		'exitFullscreen',
		'fullscreenElement',
		'fullscreenEnabled',
		'fullscreenchange',
		'fullscreenerror'
	],
	// WebKit
	[
		'webkitRequestFullscreen',
		'webkitExitFullscreen',
		'webkitFullscreenElement',
		'webkitFullscreenEnabled',
		'webkitfullscreenchange',
		'webkitfullscreenerror'
	],
	// Old WebKit (Safari 5.1)
	[
		'webkitRequestFullScreen',
		'webkitCancelFullScreen',
		'webkitCurrentFullScreenElement',
		'webkitCancelFullScreen',
		'webkitfullscreenchange',
		'webkitfullscreenerror'
	],
	// Mozilla
	[
		'mozRequestFullScreen',
		'mozCancelFullScreen',
		'mozFullScreenElement',
		'mozFullScreenEnabled',
		'mozfullscreenchange',
		'mozfullscreenerror'
	],
	// Microsoft
	[
		'msRequestFullscreen',
		'msExitFullscreen',
		'msFullscreenElement',
		'msFullscreenEnabled',
		'MSFullscreenChange',
		'MSFullscreenError'
	]
];

var specApi = apiMap[0];
var browserApi = [];

var i, c;

// determine the supported set of functions
for (i = 0, c = apiMap.length ; i < c ; ++i) {
	// check for exitFullscreen function
	if (typeof document[apiMap[i][1]] != "undefined") {
		browserApi = apiMap[i];
		break;
	}
}

// map the browser API names to the spec API names
// or leave vjs.browser.fullscreenAPI undefined
if (browserApi) {
	for (i = 0, c = browserApi.length ; i < c ; ++i) {
		FullscreenApi[specApi[i]] = browserApi[i];
	}
}







window.isFullscreen = function () {
	return  !!document[FullscreenApi.fullscreenElement];
};


/**
 * Generate a video player
 * @param idVideoElem  the ID of the HTML video element
 * @constructor
 */
var LibreVideoPlayer = function (idVideoElem) {
	'use strict';
	
	try {
		new Gauge();
	}
	catch (e) {
		throw new Error('LibreVideoPlayer require the Gauge API. Find it at https://gitlab.com/ajabep/Gauge');
	}
	
	
	// check documentation to understand where it's used ;)
	var TIMEOUT_HIDE_ALL = 2500,
		DELTA_TIME = 10,
		MAJ_MULTIPLIER_DELTA_TIME = 1.5,
		CTRL_MULTIPLIER_DELTA_TIME = 2,
		DELTA_VOLUME = 10,
		MAJ_MULTIPLIER_DELTA_VOLUME = 1.5,
		CTRL_MULTIPLIER_DELTA_VOLUME = 2;
	
	
	
	
	// some useful functions
	/**
	 * generate the Control bar ID based on the video ID
	 * @param videoID  the video ID
	 * @returns {string}  the ID of the control bar
	 */
	function genIDControl(videoID) {
		return 'LVPControl' + videoID;
	}

	/**
	 * generate the ID of the volume bar, based on the video ID
	 * @param videoID  the video ID
	 * @returns {string}  the ID of the volume bar
	 */
	function genIDVolumeBar(videoID) {
		return 'LVPVolumeBar' + videoID;
	}

	/**
	 * generate the ID of the volume control, based on the video ID
	 * @param videoID  the video ID
	 * @returns {string}  the ID of the volume control
	 */
	function genIDVolumeControl(videoID) {
		return genIDControl('Volume' + videoID);
	}

	/**
	 * check if the "hide all" is effective
	 * @returns {boolean}  true if the "hide all" is effective ; false else
	 */
	function isHideAllEffective() {
		return self.playerElem.classList.contains('hideControlBar');
	}

	/**
	 * set the "hide all"
	 */
	function hideAll() {
		self.playerElem.classList.add('hideControlBar');
	}

	/**
	 * unset the "hide all"
	 */
	function unHideAll() {
		self.playerElem.classList.remove('hideControlBar');
	}






	//object component
	this.videoElem = document.getElementById(idVideoElem);
	this.playerElem = null;
	this.progressBar = null;
	this.controls = null;
	this.requestValue = {
		isPlaying: null
	};
	
	
	
	// INIT
	
	this.videoElem.removeAttribute('controls');
	this.videoElem.setAttribute('aria-controls', genIDControl(idVideoElem));
	this.videoElem.addEventListener('ended', function () {
		self.pause();
	});
	
	// creation of the player element
	this.playerElem = document.createElement('div');
	this.playerElem.classList.add('lvpPlayer');
	this.playerElem.addEventListener(FullscreenApi.fullscreenchange, function () {
		self.updateStateFullscreen();
	});
	
	if (this.videoElem.hasAttribute('role')) {
		this.playerElem.setAttribute('role', this.videoElem.getAttribute('role'));
		this.videoElem.removeAttribute('role');
	}
	
	// and creation of teh keyCatcher to webkit browsers
	this.playerElem.keyCatcher = document.createElement('input');
	this.playerElem.keyCatcher.id = 'LVPKeyCatcher';
	this.playerElem.keyCatcher.type = 'text';
	this.playerElem.keyCatcher.setAttribute('aria-hidden', 'true');
	this.playerElem.keyCatcher.setAttribute('aria-live', 'off');
	
	// we place the player, the keyCatcher and the video element in the DOM
	this.videoElem.parentElement.replaceChild(this.playerElem, this.videoElem);
	this.playerElem.appendChild(this.videoElem);
	document.body.appendChild(this.playerElem.keyCatcher);
	
	
	// manage a timeout to hide teh mouse and the control bar if the mouse stay still
	
	var timeout;

	var listenerSetNewTimeout = function () {
		if (isHideAllEffective())
			unHideAll();

		clearTimeout(timeout);
		timeout = setTimeout(hideAll, TIMEOUT_HIDE_ALL);
	};
	this.playerElem.addEventListener('click', 					listenerSetNewTimeout);
	this.playerElem.addEventListener('dblclick',				listenerSetNewTimeout);
	this.playerElem.addEventListener('mouseup',					listenerSetNewTimeout);
	this.playerElem.addEventListener('mousedown',				listenerSetNewTimeout);
	this.playerElem.addEventListener('keydown',					listenerSetNewTimeout);
	this.videoElem.addEventListener('keydown',					listenerSetNewTimeout);
	this.playerElem.keyCatcher.addEventListener('keydown',		listenerSetNewTimeout);
	this.playerElem.addEventListener('keyup',					listenerSetNewTimeout);
	this.videoElem.addEventListener('keyup',					listenerSetNewTimeout);
	this.playerElem.keyCatcher.addEventListener('keyup',		listenerSetNewTimeout);
	this.playerElem.addEventListener('keypress',				listenerSetNewTimeout);
	this.videoElem.addEventListener('keypress',					listenerSetNewTimeout);
	this.playerElem.keyCatcher.addEventListener('keypress',		listenerSetNewTimeout);
	this.playerElem.addEventListener('focus',					listenerSetNewTimeout);
	this.playerElem.addEventListener('blur',					listenerSetNewTimeout);
	this.playerElem.addEventListener('mousemove',				listenerSetNewTimeout);
	
	this.playerElem.addEventListener('mouseout', function () {
		if (isHideAllEffective())
			unHideAll();
		
		clearTimeout(timeout);
	});
	
	
	
	// to focus the key catcher
	this.playerElem.addEventListener('click', function() {
		self.playerElem.keyCatcher.focus();
	});
	
	
	// a11y : management of the keyboard
	var ECHAP_KEY	=	27,
		SPACE_KEY	=	32,
		LEFT_KEY	=	37,
		RIGHT_KEY	=	39,
		TOP_KEY		=	38,
		BOTTOM_KEY	=	40,
		NUM_0_KEY	=	96,
		NUM_1_KEY	=	97,
		NUM_2_KEY	=	98,
		NUM_3_KEY	=	99,
		NUM_4_KEY	=	100,
		NUM_5_KEY	=	101,
		NUM_6_KEY	=	102,
		NUM_7_KEY	=	103,
		NUM_8_KEY	=	104,
		NUM_9_KEY	=	105,
		START_KEY	=	36,
		END_KEY		=	35,
		TAB_KEY		=	9,
		
		manageKey = function (e) {
            var delta, newTimePercent, newVolume, duration;
			switch(e.keyCode) {
				case ECHAP_KEY:
					if (window.isFullscreen())
						self.exitFullscreen();
					break;

				case SPACE_KEY:
					self.playPause();
					break;

				case LEFT_KEY:
                    delta = DELTA_TIME;
                    newTimePercent = self.videoElem.currentTime;
					
					if (e.shiftKey)
						delta *= MAJ_MULTIPLIER_DELTA_TIME;
					
					if (e.altKey)
						return;
					
					if (e.ctrlKey)
						delta *= CTRL_MULTIPLIER_DELTA_TIME;
					
					newTimePercent -= delta;
					
					if (newTimePercent < 0)
						newTimePercent = 0;
					
					self.goToTime( newTimePercent );
					break;

				case RIGHT_KEY:
					delta = DELTA_TIME;
                    newTimePercent = self.videoElem.currentTime;
                    duration = self.videoElem.duration;
					
					if (e.shiftKey)
						delta *= MAJ_MULTIPLIER_DELTA_TIME;
					
					if (e.altKey)
						return;
					
					if (e.ctrlKey)
						delta *= CTRL_MULTIPLIER_DELTA_TIME;
					
					newTimePercent += delta;
					
					if (newTimePercent > duration)
						newTimePercent = duration;
					
					self.goToTime( newTimePercent );
					break;

				case TOP_KEY:
					delta = DELTA_VOLUME;
                    newVolume = self.getVolume();
					
					if (e.shiftKey)
						delta *= MAJ_MULTIPLIER_DELTA_VOLUME;
					
					if (e.ctrlKey || e.altKey)
						delta *= CTRL_MULTIPLIER_DELTA_VOLUME;
					
					newVolume += delta;
					
					if (newVolume > 100)
						newVolume = 100;
					
					self.setVolume( newVolume );
					break;

				case BOTTOM_KEY:
					delta = DELTA_VOLUME;
                    newVolume = self.getVolume();
					
					if (e.shiftKey)
						delta *= MAJ_MULTIPLIER_DELTA_VOLUME;
					
					if (e.ctrlKey || e.altKey)
						delta *= CTRL_MULTIPLIER_DELTA_VOLUME;
					
					newVolume -= delta;
					
					if (newVolume < 0)
						newVolume = 0;
					
					self.setVolume( newVolume );
					break;

				case START_KEY:
				case NUM_0_KEY:
					self.goToPercent(0);
					break;

				case NUM_1_KEY:
					self.goToPercent((1/10) * 100);
					break;

				case NUM_2_KEY:
					self.goToPercent((2/10) * 100);
					break;

				case NUM_3_KEY:
					self.goToPercent((3/10) * 100);
					break;

				case NUM_4_KEY:
					self.goToPercent((4/10) * 100);
					break;

				case NUM_5_KEY:
					self.goToPercent((5/10) * 100);
					break;

				case NUM_6_KEY:
					self.goToPercent((6/10) * 100);
					break;

				case NUM_7_KEY:
					self.goToPercent((7/10) * 100);
					break;

				case NUM_8_KEY:
					self.goToPercent((8/10) * 100);
					break;

				case NUM_9_KEY:
					self.goToPercent((9/10) * 100);
					break;

				case END_KEY:
					self.goToPercent((10/10) * 100);
					break;

				case TAB_KEY:
					self.controls.playPause.focus();
					break;
			}
			
			e.preventDefault();
		};	
	
	this.playerElem.addEventListener('keydown', manageKey);
	this.playerElem.keyCatcher.addEventListener('keydown', manageKey);
	this.playerElem.addEventListener('keypress', manageKey);
	this.playerElem.keyCatcher.addEventListener('keypress', manageKey);
	

	
	// We create and place the progress bar
	// The progress bar contain the bar which show what has been played and the loading bar
	this.progressBar = new Gauge(2, ['Loaded', 'Played']);
	this.progressBar.classList.add('progressBar');
	this.progressBar.addEventListener('requestValueStart', function () {
		var playing = self.isPlaying();
		self.requestValue.isPlaying = playing;
		self.pause();
		if (playing) {
			self._setPlayState();
			self.playerElem.classList.add('requestingNewValue');
		}
	});
	this.progressBar.addEventListener('requestValue', function (e) {
		self.goToPercent(e.detail.newValue);
	});
	this.progressBar.addEventListener('requestValueStop', function () {
		if (self.requestValue.isPlaying) {
			self.play();
			self.playerElem.classList.remove('requestingNewValue');
		}
		self.requestValue.isPlaying = null;
	});
	this.playerElem.appendChild(this.progressBar);
	
	
	
	// we create an place the control bar and its controls
	
	this.controls = document.createElement('div');
	this.controls.playerState = document.createElement('span');
	this.controls.durationSpan = document.createElement('span');
	this.controls.playPause = document.createElement('button');
	this.controls.volumeGroup = document.createElement('div');
	this.controls.volumeGroup.volumeBtn = document.createElement('button');
	this.controls.volumeGroup.volumeGauge = new Gauge(1, ['volume']);
	this.controls.fullscreen = document.createElement('button');
	var volumeVideoBeforeRequest;
	
	
	// attributes and content, etc...
	this.controls.id = genIDControl(idVideoElem);
	this.controls.classList.add('controls');
	this.controls.setAttribute('aria-owns', idVideoElem);
	this.controls.setAttribute('role', 'menu');
	
	this.controls.volumeGroup.classList.add('volumeGrp');
	this.controls.volumeGroup.volumeGauge.classList.add('volumeGauge');
	
	this.controls.playPause.classList.add('playPauseBtn');
	this.controls.playPause.setAttribute('aria-labelledby', 'playPause');
	this.controls.playPause.setAttribute('title', 'playPause');
	
	this.controls.volumeGroup.volumeBtn.classList.add('volumeBtn');
	this.controls.volumeGroup.volumeBtn.setAttribute('aria-labelledby', 'mute');
	this.controls.volumeGroup.volumeBtn.id = genIDVolumeControl(idVideoElem);
	this.controls.volumeGroup.volumeBtn.setAttribute('aria-owns', genIDVolumeBar(idVideoElem));
	this.controls.volumeGroup.volumeBtn.setAttribute('title', 'mute');
	
	
	this.controls.volumeGroup.volumeGauge.id = genIDVolumeBar(idVideoElem);
	this.controls.volumeGroup.volumeGauge.setAttribute('aria-owns', genIDVolumeControl(idVideoElem));
	this.controls.volumeGroup.volumeGauge.setAttribute('aria-live', 'assertive');
	
	
	this.controls.fullscreen.classList.add('fullscreenBtn');
	this.controls.fullscreen.setAttribute('aria-labelledby', 'Fullscreen');
	this.controls.fullscreen.setAttribute('title', 'Fullscreen');
	
	
	this.controls.durationSpan.classList.add('timeCounter');
	this.controls.durationSpan.textContent = '--:-- / --:--';
	this.controls.durationSpan.setAttribute('role', 'timer');
	
	this.controls.playerState.classList.add('playerState');
	this.controls.playerState.setAttribute('role', 'status');
	this.controls.playerState.setAttribute('aria-live', 'assertive');
	
	
	// events
	
	this.controls.playPause.addEventListener('click', function () {
		self.playPause();
	});
	this.controls.volumeGroup.volumeBtn.addEventListener('click', function () {
		self.toggleMute();
	});
	this.controls.fullscreen.addEventListener('click', function () {
		self.toggleFullscreen();
	});
	
	this.controls.volumeGroup.volumeGauge.addEventListener('requestValueStart', function () {
		self.playerElem.classList.add('requestingNewValue');
		volumeVideoBeforeRequest = self.getVolume();
	});
	
	this.controls.volumeGroup.volumeGauge.addEventListener('requestValue', function (e) {
		self.setVolume(e.detail.newValue);
	});
	
	this.controls.volumeGroup.volumeGauge.addEventListener('requestValueStop', function () {
		self.playerElem.classList.remove('requestingNewValue');
		if (!self.getVolume()) { // if we have mute with the volume bar
			self.setVolume(volumeVideoBeforeRequest);
			self.mute();
		}
	});
	
	this.videoElem.addEventListener('click', function () {
		self.playPause();
	});
	this.videoElem.addEventListener('dblclick', function () {
		self.toggleFullscreen();
	});
	
	
	// + insertion in the DOM
	this.playerElem.appendChild(this.controls);
	this.controls.appendChild(this.controls.playerState);
	this.controls.appendChild(this.controls.playPause);
	this.controls.appendChild(this.controls.durationSpan);
	this.controls.volumeGroup.appendChild(this.controls.volumeGroup.volumeBtn);
	this.controls.volumeGroup.appendChild(this.controls.volumeGroup.volumeGauge);
	this.controls.appendChild(this.controls.volumeGroup);
	this.controls.appendChild(this.controls.fullscreen);
	
	
	
	
	
	
	
	// And, the method which require some globals variables
	
	LibreVideoPlayer.prototype.updateLoadTime = function () {
		
		window.requestAnimationFrame(self.updateLoadTime);

		if (!self.videoElem.buffered.length) {
			self.progressBar.setStart(0, 0);
			self.progressBar.setStart(0, 1);
			self.progressBar.setProgress(0, 0);
			return;
		}

		var start = self.videoElem.buffered.start(0) * 100,
			end = self.videoElem.buffered.end(0) * 100,
			duration = self.videoElem.duration;


		if (isNaN(duration))
			return;
		
		
		self.progressBar.setStart(start / duration, 0);
		self.progressBar.setStart(start / duration, 1);
		self.progressBar.setProgress(end / duration, 0);

	};

	LibreVideoPlayer.prototype.updateTime = function () {
		
		if( self.isPlaying ) // if we play the video, we continue to update the time
			requestAnimationFrame(self.updateTime);

		var currentTime = self.videoElem.currentTime,
			duration = self.videoElem.duration;

		if (isNaN(duration))
			return;

		// update the bar ...
		self.progressBar.setProgress(currentTime * 100 / duration, 1);

		// ... and the span
		self.controls.durationSpan.textContent = self.formatTime(currentTime) + ' / ' + self.formatTime(duration);
	};
	
	
	
	requestAnimationFrame(this.updateLoadTime);
	requestAnimationFrame(this.updateTime);
	this.updateVolumeGauge();
	
	this.setVolume(100);
	
	if (this.videoElem.hasAttribute('muted')) {
		this.controls.volumeGroup.parentNode.removeChild(this.controls.volumeGroup);
	}
	
	if (this.videoElem.hasAttribute('autoplay')) {
		this.play();
	}
	else {
		this.pause();
	}
	
	var self = this;
};

/**
 * format a time into a string
 * @param secs  the time in second
 * @return {string}  the time as a string
 */
LibreVideoPlayer.prototype.formatTime = function (secs) {
	var sec2display = Math.floor(secs % 60),
		min = Math.floor(secs / 60) % 60,
		hours = Math.floor(secs / (60 * 60)),
		returned = '';
	
	
	if (hours) {
		returned += hours;
		returned += ':';
	}
	
	if ( returned.length > 0 && min < 10 )
		returned += 0;
	
	returned += min;
	returned += ':';
	
	if ( sec2display < 10 )
		returned += 0;
	returned += sec2display;
	
	return returned.trim();
};

/**
 * Set the "playing look"
 **/
LibreVideoPlayer.prototype._setPlayState = function () {
	this.playerElem.classList.add('playing');

	this.controls.playPause.setAttribute('aria-labelledby', 'pause');
	this.controls.playPause.setAttribute('title', 'pause');

	this.controls.durationSpan.setAttribute('aria-busy', 'true');
	this.controls.playerState.textContent = 'Video playing';
};

/**
 * Set the "pause look"
 **/
LibreVideoPlayer.prototype._setPauseState = function () {
	this.playerElem.classList.remove('playing');

	this.controls.playPause.setAttribute('aria-labelledby', 'play');
	this.controls.playPause.setAttribute('title', 'play');

	this.controls.durationSpan.setAttribute('aria-busy', 'false');
	this.controls.playerState.textContent = 'Video paused';
};

/**
 * play the video
 */
LibreVideoPlayer.prototype.play = function () {
	this.videoElem.play();
	this._setPlayState();
	requestAnimationFrame(this.updateTime);
};

/**
 * pause the video
 */
LibreVideoPlayer.prototype.pause = function () {
	this.videoElem.pause();
	this._setPauseState();
	// no cancelAnimationFrame : we haven't wet update the played time
};

/**
 * toggle play/pause
 */
LibreVideoPlayer.prototype.playPause = function () {
	if (this.isPlaying())
		this.pause();
	else
		this.play();
};

/**
 * stop the video
 */
LibreVideoPlayer.prototype.stop = function () {
	this.pause();
	this.videoElem.currentTime = 0;
};

/**
 * get the volume of the video
 * @returns {number}  the volume in percent
 */
LibreVideoPlayer.prototype.getVolume = function () {
	return this.videoElem.volume * 100;
};

/**
 * set the volume of the video
 * @param volume  the volume in percent
 */
LibreVideoPlayer.prototype.setVolume = function (volume) {
	if (volume < 0 || volume > 100) {
		throw new Error('the first param must be between 0 and 100');
	}
	
	this.videoElem.volume = volume / 100;
	
	if (volume === 0) {
		this.playerElem.classList.add('mute');
		this.controls.volumeGroup.volumeBtn.setAttribute('aria-labelledby', 'unmute');
		this.controls.volumeGroup.volumeBtn.setAttribute('title', 'unmute');
	}
	else {
		this.playerElem.classList.remove('mute');
		this.controls.volumeGroup.volumeBtn.setAttribute('aria-labelledby', 'mute');
		this.controls.volumeGroup.volumeBtn.setAttribute('title', 'mute');
	}
	
	if (volume < 50)
		this.playerElem.classList.add('lowVolume');
	else
		this.playerElem.classList.remove('lowVolume');
	
	this.updateVolumeGauge();
};

/**
 * mute the video
 */
LibreVideoPlayer.prototype.mute = function () {
	this.volumeBeforeMute = this.getVolume();
	this.setVolume(0);
};

/**
 * unmute the video
 */
LibreVideoPlayer.prototype.unmute = function () {
	this.setVolume(this.volumeBeforeMute);
	this.volumeBeforeMute = undefined;
};

/**
 * check if the video is muted
 * @returns {boolean}  true if the video is muted ; false else
 */
LibreVideoPlayer.prototype.isMuted = function () {
	return typeof this.volumeBeforeMute !== 'undefined';
};

/**
 * toggle the mute
 */
LibreVideoPlayer.prototype.toggleMute = function () {
	if (this.isMuted())
		this.unmute();
	else
		this.mute();
};

/**
 * update the volume gauge
 */
LibreVideoPlayer.prototype.updateVolumeGauge = function () {
	this.controls.volumeGroup.volumeGauge.setProgress(this.videoElem.volume * 100);
};

/**
 * request the fullscreen to the video
 */
LibreVideoPlayer.prototype.fullscreen = function () {
	this.playerElem[FullscreenApi.requestFullscreen]();
	this.controls.playPause.setAttribute('aria-labelledby', 'fullscreen');
	this.controls.playPause.setAttribute('title', 'fullscreen');
};

/**
 * exit of the fullscreen
 */
LibreVideoPlayer.prototype.exitFullscreen = function () {
	document[FullscreenApi.exitFullscreen]();
	this.controls.playPause.setAttribute('aria-labelledby', 'exit fullscreen');
	this.controls.playPause.setAttribute('title', 'exit fullscreen');
};

/**
 * toggle the fullscreen
 */
LibreVideoPlayer.prototype.toggleFullscreen = function () {
	if( window.isFullscreen() )
		this.exitFullscreen();
	else
		this.fullscreen();
};

/**
 * update the "fullscreen look"
 */
LibreVideoPlayer.prototype.updateStateFullscreen = function () {
	if( window.isFullscreen() )
		this.playerElem.classList.add('fullscreen');
	else
		this.playerElem.classList.remove('fullscreen');
};

/**
 * progress the video until a time
 * @param time  the time in second
 */
LibreVideoPlayer.prototype.goToTime = function (time) {
	if (time < 0)
		throw new Error('the wanted time can\'t be negative');
	
	if (this.videoElem.duration < time)
		throw new Error('the wanted time can\'t exceed the total duration');
	
	var playing = this.isPlaying();
	if (!playing) {
		this.videoElem.play()
	}
	
	this.videoElem.currentTime = time;
	
	if (!playing) {
		this.videoElem.pause()
	}
};

/**
 * progress the video until a percentage of the video
 * @param percent  the time in percent
 */
LibreVideoPlayer.prototype.goToPercent = function (percent) {
	if (percent < 0)
		throw new Error('the wanted percentage can\'t be negative');
	
	if (percent > 100)
		throw new Error('the wanted percentage can\'t exceed 100');
	
	this.goToTime(this.videoElem.duration * percent / 100);
};

/**
 * check if the video is playing
 * @returns {boolean} true if the video is playing ; false else
 */
LibreVideoPlayer.prototype.isPlaying = function () {
    console.log(this.videoElem.paused, this.videoElem)
	return !this.videoElem.paused;
};

