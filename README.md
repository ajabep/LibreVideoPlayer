LibreVideoPlayer
================

LibreVidePlayer is a cool API to manage a video player for the web.

Easy to use, accessible, ergonomic, intuitive, this player embed all the feature you can want ^^


Accessibility
-------------

This video player is fully accessible.

It respect the ARIA norm.

More, the colors always respects the WCAG2.0 


Interface
---------

The interface is made to be ergonomic and intuitive :
I have refine it to have just what you need and mae it easy-to-use.
Some functions are available with the keyboard.

Icons
-----

The icons are form [Open Iconic](https://useiconic.com/open) and manage with [IcoMoon](https://icomoon.io/)


License
-------

LibreVideoPlayer is under MIT License ;)
